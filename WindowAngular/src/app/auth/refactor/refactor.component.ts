import { Component, OnInit } from '@angular/core';
import { getrefactormodel } from './refactormodel';
import { AuthService } from '../../auth.service';
import { Router } from '@angular/router';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Observable } from "rxjs";
import { SelectionModel } from '@angular/cdk/collections';
import {DataSource} from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material';
import {MatDialog, MatDialogConfig} from "@angular/material";
import {DialogRefactorComponent} from "../dialog-refactor/dialog-refactor.component";
@Component({
  selector: 'app-refactor',
  templateUrl: './refactor.component.html',
  styleUrls: ['./refactor.component.scss']
})
export class RefactorComponent implements OnInit {
  displayedColumns  = ["select", "name" ,"tipalabra", "renowned_word" ,"word_to_rename","activecheck", "ruta"];
  myDataArray: getrefactormodel[] = [];
  dataSource = new MatTableDataSource<getrefactormodel>();
  selection = new SelectionModel<getrefactormodel>(true, []);
  constructor(private dialog: MatDialog, private fb: FormBuilder, private router: Router, private authService: AuthService) {
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    // if there is a selection then clear that selection
    if (this.isAllSelected()) {
      this.selection.clear();
    } else {
      this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
    }
}

editSelectRow(){
  if(this.selection.selected.length==1){

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data=this.selection.selected[0];
    this.dialog.open(DialogRefactorComponent, dialogConfig);
    const dialogRef = this.dialog.open(DialogRefactorComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(data => {
    
          if(data){
            this.editRowData(data);
          }
            
          this.dialog.closeAll();
          this.selection = new SelectionModel<getrefactormodel>(true, []); 
        }
        
    ); 
    
  }else{
    alert('Solo se puede editar una fila, marque solo una fila para editarla');
  }
}
  removeSelectedRows() {
   
    this.selection.selected.forEach(item => {
      let index: number = this.dataSource.data.findIndex(d => d === item);
      console.log(this.dataSource.data.findIndex(d => d === item));
      this.dataSource.data.splice(index,1)
      this.dataSource = new MatTableDataSource<getrefactormodel>(this.dataSource.data);
         
    });
    this.selection = new SelectionModel<getrefactormodel>(true, []);
  }

  openDialog() {
 
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    this.dialog.open(DialogRefactorComponent, dialogConfig);
    const dialogRef = this.dialog.open(DialogRefactorComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(data => {
          if(data){
            this.addRowData(data);
          }
          this.dialog.closeAll();
           
        }
        
    );   
    
    
}

editRowData(row_obj){
  let index: number = this.dataSource.data.findIndex(d => d === this.selection.selected[0]);
  
 
 //this.dataSource = new MatTableDataSource<getrefactormodel>(this.dataSource.data);
  this.dataSource.data[index]=row_obj;

  
  this.dataSource._updateChangeSubscription();
}


addRowData(row_obj){
 
  this.dataSource.data.push(row_obj);
   
  this.dataSource._updateChangeSubscription();
}


ngOnInit() {
  
 this.authService.getrefactor().subscribe((res : getrefactormodel[])=>{
    this.myDataArray = res;
    this.dataSource = new MatTableDataSource();  
    this.dataSource.data = this.myDataArray;  
  });
  } 
  
  
  submitRefac(){
   this.authService.refactor(this.dataSource.data)
         .subscribe(res => {
           this.router.navigate(['refactor']);
           location.reload();
         }, (err) => {
           console.log(err);
           alert(err.error);
         });
     }
  }
 


