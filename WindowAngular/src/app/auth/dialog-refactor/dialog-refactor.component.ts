import { Component, OnInit ,Inject, Optional, ɵNOT_FOUND_CHECK_ONLY_ELEMENT_INJECTOR } from '@angular/core';
import { AuthService } from '../../auth.service';
import {MatDialogRef,MAT_DIALOG_DATA} from "@angular/material";
import {FormControl,FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import{getrefactormodel} from "../refactor/refactormodel";
import { opt } from './doctypemodel';
import { Router } from '@angular/router';
import { getmovemodel } from '../move/movemodel';
@Component({
  selector: 'app-dialog-refactor',
  templateUrl: './dialog-refactor.component.html',
  styleUrls: ['./dialog-refactor.component.scss']
})
export class DialogRefactorComponent implements OnInit {
  valueTipDoc:number;
  form: FormGroup;
  optPath: opt[]=[];
  moveByDoc: getmovemodel[]=[];
  arrayPath :getmovemodel[]=[];
  constructor(
    private router: Router,
      private fb: FormBuilder,private authService: AuthService,
      private dialogRef: MatDialogRef<DialogRefactorComponent>,
      @Optional() @Inject(MAT_DIALOG_DATA) public data1:getrefactormodel
      ) {
        this.authService.getdoctype().subscribe((res : opt[])=>{
          this.optPath = res;
          
        });
        
  }
 
  ngOnInit() {
   
 this.form = this.fb.group({
      id:['' ],
      word_to_rename: ['', Validators.required],
      renowned_word: ['', Validators.required],
      name:['', Validators.required],
      activecheck:['' , Validators.required],
      tipalabra:['', Validators.required],
      idPath:[],
      valueTipDoc:['']
      
    });


    this.form.get('activecheck').setValue('D');

    if(this.data1!=null){
      this.form.get('activecheck').setValue(this.data1.activecheck);
      this.form.get('word_to_rename').setValue(this.data1.word_to_rename);
      this.form.get('id').setValue(this.data1.id);
      this.form.get('renowned_word').setValue(this.data1.renowned_word);
      this.form.get('name').setValue(this.data1.name);
      this.form.get('tipalabra').setValue(this.data1.tipalabra);
      this.form.get('valueTipDoc').setValue(this.data1.idPath[0].doctype);
      this.loadPath(this.data1.idPath[0].doctype);
  
      const idsPath = this.data1.idPath.map(x => x.id);
 
     this.form.get('idPath').setValue(idsPath);
      
     
    }

    
    
  }

loadPath(idDoc){
  this.authService.getmoveByDoc(idDoc)
  .subscribe((res : getmovemodel[])=>{
    this.moveByDoc = res; 
  }); 
}



  save() {
    if(this.form.get('idPath').value){
      for(let i=0;this.moveByDoc.length>i;i++){
        for(let e=0;this.form.value.idPath.length>e;e++){
           if(this.moveByDoc[i].id==this.form.value.idPath[e]){
               this.arrayPath.push(this.moveByDoc[i]);
   
           }
        }
      }
      this.form.get('idPath').setValue(this.arrayPath);
    }
    
   

    if(this.form.valid){
      this.dialogRef.close(this.form.value);
      }
      else{
        alert('Texto mal introducido');
      }
    
  }

  close() {
      this.dialogRef.close();
  } 
  
}
