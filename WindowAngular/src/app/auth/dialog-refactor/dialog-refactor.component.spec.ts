import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogRefactorComponent } from './dialog-refactor.component';

describe('DialogRefactorComponent', () => {
  let component: DialogRefactorComponent;
  let fixture: ComponentFixture<DialogRefactorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogRefactorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogRefactorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
