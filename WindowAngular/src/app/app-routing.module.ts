import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RefactorComponent } from './auth/refactor/refactor.component';
import { MoveComponent } from './auth/move/move.component';

const routes: Routes = [ {
    path: '',
    redirectTo: 'refactor',
    pathMatch: 'full'
  },
  {
    path: 'refactor',
    component: RefactorComponent,
    data: { title: 'Refactor' }
  },
{
    path: 'move',
    component: MoveComponent,
    data: { title: 'Move' }
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
