package com.serikat.pantalla.converters;

import java.util.ArrayList;

import java.util.List;


import org.springframework.stereotype.Component;

import com.serikat.pantalla.entity.DoctypeEntity;


import com.serikat.pantalla.model.Doctype;



@Component
public class DoctypeConverter  {
	// modelEntity --> model
		public List<Doctype> convertEntyModel(List<DoctypeEntity> entityD) {

			List<Doctype> doctype = new ArrayList<>();
			for (DoctypeEntity entyD : entityD) {
				Doctype doc = new Doctype();
				doc.setId(entyD.getId());
				doc.setName(entyD.getName());
				doc.setPattern(entyD.getPattern());
				doctype.add(doc);
			}

			return doctype;
		}

		
}
