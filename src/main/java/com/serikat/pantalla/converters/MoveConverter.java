package com.serikat.pantalla.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.serikat.pantalla.entity.MoveEntity;
import com.serikat.pantalla.model.Move;

// Transforma los modelos en entidades para la bbdd y viceversa.

@Component
public class MoveConverter implements Converter<Move, MoveEntity> {

	// model --> modelEntity
	@Override
	public MoveEntity convert(Move move) {

		MoveEntity moveEntity = new MoveEntity();
		moveEntity.setOriginPath(move.getOriginpath());
		moveEntity.setTargetPath(move.getTargetpath());
		moveEntity.setId(move.getId());
		moveEntity.setDoctype(move.getDoctype());

		return moveEntity;
	}

	// modelEntity --> model
	public List<Move> convertEntyModel(List<MoveEntity> entityM) {

		List<Move> move = new ArrayList<>();
		for (MoveEntity entyM : entityM) {
			Move mov = new Move();
			mov.setId(entyM.getId());
			mov.setOriginpath(entyM.getOriginPath());
			mov.setTargetpath(entyM.getTargetPath());
			mov.setDoctype(entyM.getDoctype());
			move.add(mov);
		}

		return move;
	}

	// List entity --> List model
	public Move convertEntyModel2(MoveEntity entityM) {

		Move mov = new Move();

		mov.setId(entityM.getId());
		mov.setOriginpath(entityM.getOriginPath());
		mov.setTargetpath(entityM.getTargetPath());
		mov.setDoctype(entityM.getDoctype());
		return mov;

	}

}
