package com.serikat.pantalla.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.serikat.pantalla.entity.WordsEntity;
import com.serikat.pantalla.entity.MoveEntity;
import com.serikat.pantalla.model.Move;
import com.serikat.pantalla.model.Refactor;


@Component
public class RulesConverter implements Converter<Refactor, WordsEntity>{
	@Autowired
	private MoveConverter moveConverter;

	//model --> entity
	@Override
    public WordsEntity convert(Refactor refactor) {
		WordsEntity rulesEntity = new WordsEntity();
		rulesEntity.setSearchType(refactor.getWord_to_rename());
		rulesEntity.setChangeType(refactor.getRenowned_word());
		rulesEntity.setName(refactor.getName());
		rulesEntity.setId(refactor.getId());
		rulesEntity.setStatus(refactor.getActivecheck());
		rulesEntity.setType(refactor.getTipalabra());
		List<MoveEntity> rutas=new ArrayList<>();
		for(Move m:refactor.getIdPath()) {
			rutas.add(moveConverter.convert(m));
		}
		rulesEntity.setRutas(rutas);
        return rulesEntity;
    }
	
	//entity --> model
	public Refactor convertEntyModel2(WordsEntity entity) {
	        Refactor refac = new Refactor();
			refac.setId(entity.getId());
			refac.setWord_to_rename(entity.getSearchType());
			refac.setRenowned_word(entity.getChangeType());
			refac.setName(entity.getName());
			refac.setActivecheck(entity.getStatus());
			refac.setTipalabra(entity.getType());
			refac.setIdPath(moveConverter.convertEntyModel(entity.getRutas()));
			return refac;
	}

	//List entity --> List model
	public List<Refactor> convertEntyModel(List<WordsEntity> entity) {
		List<Refactor> refactor = new ArrayList<>();
		for(WordsEntity enty : entity) {
			Refactor refac = new Refactor();
			refac.setId(enty.getId());
			refac.setWord_to_rename(enty.getSearchType());
			refac.setRenowned_word(enty.getChangeType());
			refac.setName(enty.getName());
			refac.setActivecheck(enty.getStatus());
			refac.setTipalabra(enty.getType());
			refac.setIdPath(moveConverter.convertEntyModel(enty.getRutas()));
			refactor.add(refac);
		}
		
        return refactor;
	}
}
