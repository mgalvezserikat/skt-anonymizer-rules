package com.serikat.pantalla.service;

import java.util.List;

import com.serikat.pantalla.model.Move;
//import com.serikat.pantalla.entity.MoveEntity;

public interface MoveService {

	Move saveMove(Move move);

	List<Move> listAllMove();

	// List<Move> antes era Move[]
	void deleteMove(List<Move> move);

	Move updatemove(Move move);


	List<Move> listMoveByIdDoc(Long idDoc);

}
