package com.serikat.pantalla.service;

import java.util.List;


import com.serikat.pantalla.model.Refactor;

public interface RefactorService {
	Refactor saveRefactor(Refactor refactor);

	List<Refactor> listAllRules();

	void deleteRefactor(List<Refactor> refactor);

	Refactor updateRefactor(Refactor refactor1);
}
