package com.serikat.pantalla.service;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.serikat.pantalla.converters.RulesConverter;
import com.serikat.pantalla.entity.WordsEntity;
import com.serikat.pantalla.model.Refactor;
import com.serikat.pantalla.repository.WordRepository;



@Service
public class RefactorServiceImpl implements RefactorService{

	@Autowired  
	private WordRepository refactorRepository;
	
	@Autowired  
	private RulesConverter rulesConverter;
	

	//Guardado en BBDD de Rules
	@Override
	 public Refactor saveRefactor(Refactor refactor) {
		WordsEntity entity= new WordsEntity();
		entity=rulesConverter.convert(refactor);
		entity = refactorRepository.save(entity);
		return rulesConverter.convertEntyModel2(entity);
	    }
	
	
	//Listado de Rules
	@Override
	 public List<Refactor> listAllRules() {
	    List<WordsEntity> entity = new ArrayList<>();
	
		entity.addAll((Collection<? extends WordsEntity>) refactorRepository.findAll());
		List<Refactor> refactor= new ArrayList<>();
		refactor=rulesConverter.convertEntyModel(entity);
        return refactor;
	    }

	//Borra las filas de Rules en la base de datos
	@Override
	public void deleteRefactor(List<Refactor> refactor) {
		List<WordsEntity> entity= new ArrayList<>();
		for(Refactor ref: refactor) {
			entity.add(rulesConverter.convert(ref));
		}
		
		
		refactorRepository.deleteAll(entity);
		
	}

	
	//Actualiza Tabla Rules con las filas modificadas
	@Override
	public  Refactor updateRefactor(Refactor refactor1) {
		WordsEntity entity= new WordsEntity();
		entity=rulesConverter.convert(refactor1);
		//entity.setId(null);
		entity= refactorRepository.save(entity);
		return rulesConverter.convertEntyModel2(entity);
	}
}
