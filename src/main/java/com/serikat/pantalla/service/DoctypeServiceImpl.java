package com.serikat.pantalla.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.serikat.pantalla.converters.DoctypeConverter;

import com.serikat.pantalla.entity.DoctypeEntity;

import com.serikat.pantalla.model.Doctype;

import com.serikat.pantalla.repository.DoctypeRepository;


@Service
public class DoctypeServiceImpl implements DoctypeService {

	@Autowired
	private DoctypeRepository doctypeRepository;

	@Autowired
	private DoctypeConverter doctypeConverter;

	
	// Listado de moves
	@Override
	public List<Doctype> listAllDoctype() {

		List<DoctypeEntity> entityD = new ArrayList<>();
		entityD.addAll((Collection<? extends DoctypeEntity>) doctypeRepository.findAll());
		List<Doctype> doctype = new ArrayList<>();
		doctype = doctypeConverter.convertEntyModel(entityD);

		return doctype;

	}


	

	

}
