package com.serikat.pantalla.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.serikat.pantalla.converters.MoveConverter;
import com.serikat.pantalla.entity.MoveEntity;
import com.serikat.pantalla.model.Move;
import com.serikat.pantalla.repository.MoveRepository;

@Service
public class MoveServiceImpl implements MoveService {

	
	@Autowired
	private MoveRepository moveRepository;

	@Autowired
	private MoveConverter moveConverter;

	// Guardado en BBDD de moves
	@Override
	public Move saveMove(Move move) {

		MoveEntity entityM = new MoveEntity();
		entityM = moveConverter.convert(move);
		moveRepository.save(entityM);

		return moveConverter.convertEntyModel2(entityM);

	}

	// Listado de moves
	@Override
	public List<Move> listAllMove() {

		List<MoveEntity> entityM = new ArrayList<>();
		entityM.addAll((Collection<? extends MoveEntity>) moveRepository.findAll());
		List<Move> move = new ArrayList<>();
		move = moveConverter.convertEntyModel(entityM);

		return move;

	}

	// Borra las filas de moves en la bbdd
	@Override
	public void deleteMove(List<Move> move) {

		List<MoveEntity> entity = new ArrayList<>();
		for (Move ref : move) {
			entity.add(moveConverter.convert(ref));
		}
		moveRepository.deleteAll(entity);

	}

	// Actualiza la tabla de userpaths con las filas modificadas
	@Override
	public Move updatemove(Move move) {

		MoveEntity entityM = new MoveEntity();
		entityM = moveConverter.convert(move);
		entityM = moveRepository.save(entityM);

		return moveConverter.convertEntyModel2(entityM);

	}

	@Override
	public List<Move> listMoveByIdDoc(Long idDoc) {
		List<Move> mov= listAllMove();
		List<Move> moveDoc= new ArrayList();
		for(Move m1:mov) {
			if(m1.getDoctype()==idDoc) {
				moveDoc.add(m1);
			}
		}
		
		
		
		return moveDoc;
	}

}
