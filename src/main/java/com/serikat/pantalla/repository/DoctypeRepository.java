package com.serikat.pantalla.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.serikat.pantalla.entity.DoctypeEntity;


@Repository
public interface DoctypeRepository extends CrudRepository<DoctypeEntity, Long> {

}