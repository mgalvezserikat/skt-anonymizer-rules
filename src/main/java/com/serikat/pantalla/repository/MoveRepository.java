package com.serikat.pantalla.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.serikat.pantalla.entity.MoveEntity;

@Repository
public interface MoveRepository extends CrudRepository<MoveEntity, Long> {

}
