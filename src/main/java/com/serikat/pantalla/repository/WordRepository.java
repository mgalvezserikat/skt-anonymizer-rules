package com.serikat.pantalla.repository;
import com.serikat.pantalla.entity.WordsEntity;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WordRepository extends CrudRepository<WordsEntity,Long> {


}