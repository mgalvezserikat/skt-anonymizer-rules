package com.serikat.pantalla.entity;
import javax.persistence.JoinColumn;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(  name = "rules",schema="nymiz" )
public class WordsEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "searchtype", nullable = false, length = 255)
	private String searchType;

	@Column(name = "changetype", nullable = false, length = 255)
	private String changeType;

	@Column(name = "namerule", nullable = false, length = 255)
	private String name;
	
	@Column(name = "status", nullable = false, length = 1)
	private String status;

	@Column(name = "type", nullable = false, length = 1)
	private String type;

	
	@ManyToMany
	@JoinTable(name = "userpathsrules", schema = "nymiz",
	        joinColumns = @JoinColumn(name = "idrules"),
	        inverseJoinColumns = @JoinColumn(name = "iduserpath")
	    )
	private List<MoveEntity> rutas = new ArrayList<>();
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getChangeType() {
		return changeType;
	}

	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<MoveEntity> getRutas() {
		return rutas;
	}

	public void setRutas(List<MoveEntity> rutas) {
		this.rutas = rutas;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	

}
