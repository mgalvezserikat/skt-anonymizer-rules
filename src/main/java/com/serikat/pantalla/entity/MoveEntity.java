package com.serikat.pantalla.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "userpaths", schema = "nymiz")
public class MoveEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "originpath", nullable = false, length = 255)
	private String originPath;

	@Column(name = "targetpath", nullable = false, length = 255)
	private String targetPath;
	
	@Column(name = "doctype")
	private Long doctype;
	
	

	//No creo que haga falta
	/*
	 * @ManyToMany(mappedBy = "rutas") private List<WordsEntity> rules = new
	 * ArrayList<>();
	 */
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOriginPath() {
		return originPath;
	}

	public void setOriginPath(String originPath) {
		this.originPath = originPath;
	}

	public String getTargetPath() {
		return targetPath;
	}

	public void setTargetPath(String targetPath) {
		this.targetPath = targetPath;
	}

	public Long getDoctype() {
		return doctype;
	}

	public void setDoctype(Long doctype) {
		this.doctype = doctype;
	}
	
	

}
