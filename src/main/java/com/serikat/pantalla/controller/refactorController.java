package com.serikat.pantalla.controller;

import static org.springframework.http.ResponseEntity.ok;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.serikat.pantalla.model.Doctype;
import com.serikat.pantalla.model.Move;
import com.serikat.pantalla.model.Refactor;
import com.serikat.pantalla.service.DoctypeService;
import com.serikat.pantalla.service.MoveService;
import com.serikat.pantalla.service.RefactorService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/ref")
public class refactorController {

	@Autowired
	private RefactorService refactorService;
	@Autowired
	private MoveService moveService;
	@Autowired
	private DoctypeService doctypeService;

	@SuppressWarnings("rawtypes")
	@PostMapping("/rename")
	public ResponseEntity rename(@RequestBody Refactor[] refactor) {

		// Guardado de filas nuevas y se actualizan las existentes
		for (int i = 0; i < refactor.length; i++) {
			
			if (refactor[i].getId() == null) {
				refactor[i] = refactorService.saveRefactor(refactor[i]);
			} else {
				refactor[i] = refactorService.updateRefactor(refactor[i]);
			}
		}

		// Capturar las filas que han borrado
		List<Refactor> refacdelete = new ArrayList<>();
		refacdelete = refactorService.listAllRules();
		for (Refactor refactor1 : refactor) {
			refacdelete= forexit(refactor1,refacdelete);
		}

		refactorService.deleteRefactor(refacdelete);
		Map<Object, Object> model = new HashMap<>();
		model.put("message", "Words registered successfully");
		return ok(model);
	}
	public List<Refactor>  forexit(Refactor refactor1, List<Refactor> refacdelete) {
		for (int i = 0; i < refacdelete.size(); i++) {
			if (refacdelete.get(i).equals(refactor1)) {
				refacdelete.remove(i);
				return  refacdelete;
			}
		}
		return refacdelete;
	}

	@SuppressWarnings("rawtypes")
	@PostMapping("/move")
	public ResponseEntity move(@RequestBody Move[] move) {
		// Guardado de filas nuevas y se actualizan las existentes
		for (int i = 0; i < move.length; i++) {
			if (move[i].getId() == null) {
				move[i] = moveService.saveMove(move[i]);
			} else {
				move[i] = moveService.updatemove(move[i]);
			}
		}

		// Capturar las filas que han borrado
		List<Move> movdelete = new ArrayList<>();
		movdelete = moveService.listAllMove();
		for (int i = 0; i < movdelete.size(); i++) {
			for (Move move2 : move) {
				if (movdelete.get(i).equals(move2)) {
					movdelete.remove(i);
				}
			}
		}

		// Conflicto entre List<Move> y Move[] en deleteMove()
		moveService.deleteMove(movdelete);

		Map<Object, Object> model = new HashMap<>();
		model.put("message", "Paths registered successfully");
		return ok(model);
	}

	@GetMapping("/getRules")
	public List<Refactor> getRules() {

		// Lista de las rules de la BBDD para devolver a angular
		List<Refactor> refac = new ArrayList<>();
		refac = refactorService.listAllRules();

		return refac;
	}
	
	@GetMapping("/getDoctype")
	public List<Doctype> getDoctypes() {

		List<Doctype> doc = new ArrayList<>();
		doc = doctypeService.listAllDoctype();

		return doc;

	}
	
	@PostMapping(path = "/getPathByDoc")
	public List<Move> getPathByDoc(@RequestBody Long id) {
		
		List<Move> doc = new ArrayList<>();
		doc = moveService.listMoveByIdDoc(id);

		return doc;

	}

	@GetMapping("/getMoves")
	public List<Move> getMoves() {

		List<Move> refac = new ArrayList<>();
		refac = moveService.listAllMove();

		return refac;

	}

}
