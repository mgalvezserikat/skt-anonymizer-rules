package com.serikat.pantalla.model;

public class Doctype {
Long id;
String name;
String pattern;
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getPattern() {
	return pattern;
}
public void setPattern(String pattern) {
	this.pattern = pattern;
}
public Doctype(Long id, String name, String pattern) {
	super();
	this.id = id;
	this.name = name;
	this.pattern = pattern;
}
public Doctype() {
	super();
	// TODO Auto-generated constructor stub
}


}
