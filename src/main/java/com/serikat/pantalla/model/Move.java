package com.serikat.pantalla.model;

public class Move {
	
	Long id;
	String originpath;
	String targetpath;
	String name;
	Long doctype;
	

	public String getOriginpath() {
		return originpath;
	}
	public void setOriginpath(String originpath) {
		this.originpath = originpath;
	}
	
	public String getTargetpath() {
		return targetpath;
	}
	public void setTargetpath(String targetpath) {
		this.targetpath = targetpath;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getDoctype() {
		return doctype;
	}
	public void setDoctype(Long doctype) {
		this.doctype = doctype;
	}
	public Move(Long id, String originpath, String targetpath, String name, Long doctype) {
		super();
		this.id = id;
		this.originpath = originpath;
		this.targetpath = targetpath;
		this.name = name;
		this.doctype = doctype;
	}
	public Move() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((doctype == null) ? 0 : doctype.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((originpath == null) ? 0 : originpath.hashCode());
		result = prime * result + ((targetpath == null) ? 0 : targetpath.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Move other = (Move) obj;
		if (doctype == null) {
			if (other.doctype != null)
				return false;
		} else if (!doctype.equals(other.doctype))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (originpath == null) {
			if (other.originpath != null)
				return false;
		} else if (!originpath.equals(other.originpath))
			return false;
		if (targetpath == null) {
			if (other.targetpath != null)
				return false;
		} else if (!targetpath.equals(other.targetpath))
			return false;
		return true;
	}
	
	
	
}
